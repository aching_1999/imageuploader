<?php
	require_once("config.php");
	
	$conn = new mysqli($db_host, $db_user, $db_password, $db_name);
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}
	
	//handle post actions
	if(isset($_POST["action"])) {
		$action = $_POST["action"];
		switch($action) {
			case "upload":
			case "update":
				//handle file upload	
				$error_msg = "";
				$target_file = "";
				$fileUploaded = false;
				$has_file = $_FILES["file"]["name"] != "" ? true : false;
				if($action == "upload" || ($action == "update" && $has_file == true)) {
					$fileUploaded = checkUploadedFile($target_file, $error_msg, $upload_folder);
				} 
				
				if ($fileUploaded == false && $has_file == true) {
					  $error_msg .= "File wasn't uploaded. ";
				} else {
					$base_filename = "";
					$proceed_to_update = true;
					if($has_file == true) {
					  if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {													
					  	$base_filename = basename( $_FILES["file"]["name"]);
					  } else {
						$error_msg = "There was an error uploading your file.";
						$proceed_to_update = false;
					  }
					} 
					
					if($proceed_to_update == true) {
						$title = $_POST["title"];
						switch($action) {
							case "upload":
								$sql = "INSERT INTO uploads (title, filename) VALUES ('".$title."', '".$base_filename."')";
								if ($conn->query($sql) === TRUE) {
								  $error_msg = "Record ". htmlspecialchars($base_filename). " has been created.";
								} else {
								  $error_msg =  "There was an error in creating the record: " . $conn->error;
								}								
								break;
							case "update":
								$id = $_POST["id"];
								if($base_filename != "")
									$sql = "UPDATE uploads SET title = '".$title."', filename = '".$base_filename."', 
									               date_modified = CURRENT_TIMESTAMP()  
									        WHERE id = ".$id;
								else
									$sql = "UPDATE uploads SET title = '".$title."' , date_modified = CURRENT_TIMESTAMP()  
									        WHERE id = ".$id;
								if ($conn->query($sql) === TRUE) {
								  $error_msg = "Record  has been updated.";
								} else {
								  $error_msg =  "There was an error in updating the record: " . $conn->error;
								}
								break;
						}
					}					
				}
				die($error_msg);			
				break;
				
			case "delete":
				$id = $_POST["id"];
				$sql = "DELETE FROM uploads WHERE id = ".$id;
				$result = $conn->query($sql);				
				if ($conn->query($sql) === TRUE) {
					$error_msg = "Record  has been deleted.";
				} else {
					$error_msg =  "There was an error in deleting the record: " . $conn->error;
				}
				die($error_msg);
				break;
			
			case "list":
				//list file uploads
				$data = array();
				$sql = "SELECT * FROM uploads ORDER BY id DESC";
				$uploads = $conn->query($sql);				
				if ($uploads->num_rows > 0) {
				   while($upload = $uploads->fetch_assoc()) {
				   	$data[] = $upload;
				   }
				}
				die(json_encode($data));
				break;
		}
	}
	
	function checkUploadedFile(&$target_file, &$error_msg, $upload_folder) {
		$error_msg = "There was an error uploading your file.";
		$fileUploaded = false;
		$target_dir = $upload_folder;
		$target_file = $target_dir . basename($_FILES["file"]["name"]);
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));				
		
		$fileCheck = getimagesize($_FILES["file"]["tmp_name"]);
		if($fileCheck !== false) {
			$fileUploaded = true;		  
		} else {
			$error_msg = "Not a valid image file. ";
		}
		  
		if ($_FILES["file"]["size"] > 5000000) {
			  $fileUploaded = false;
			  $error_msg .= "Upload exceeded 5MB. ";
		}
		  
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" && $imageFileType != "svg" && $imageFileType != "bmp" ) {
			  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			  $fileUploaded = false;
			  $error_msg .= "Not a valid image file type. ";
		}
		
		return $fileUploaded;
	}

	
	$conn->close();	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Image Uploader</title>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script>
	function doDelete(id, title, filaname) {
		if(confirm("Delete the selected record?")) {
			$.ajax({
				method: 'POST',
				url: window.location.pathname,
				data: {
					'action': 'delete',
					'id': id
				}
			}).done(function(data) {
				alert(data);
				fetchUploads();
			});
		}
	}
	
	function doUpdate(id, title, filaname) {
		$("#id").val(id);
		$("#title").val(title);
		$("#thumb").attr('src', 'uploads/'+filaname);
		$("#updateDialog").dialog("open");
	}
	
	function fetchUploads() {
		$.ajax({
			method: 'POST',
			url: window.location.pathname,
			data: {
				'action': 'list'
			}
		}).done(function(data) {
			$("#list-body").html();
			var json_data = JSON.parse(data);
			var list_count = 0;
			var html_output = "";
			var upload_folder = "<?=$upload_folder?>";
			$.each(json_data, function(key, media) {
				list_count++;
				html_output += `
					<tr>
						<td>${media.title}</td>
						<td><a class="media-photo-container" href="${upload_folder + media.filename}" target="_blank"><img class="media-photo" src="${upload_folder + media.filename}" /></a></td>
						<td>${media.filename}</td>
						<td>${media.date_modified}</td>
						<td><button type="button" class="btn delete" title="Delete" onclick="doDelete(${media.id}, '${media.title}','${media.filename}')">Delete</button>
							<button type="button" class="btn update" title="Edit" onclick="doUpdate(${media.id}, '${media.title}','${media.filename}')">Update</button>
						</td>
					</tr>
				`;
			});
			$("#list-body").html(html_output);
		});
		$("#uploadDialog,#updateDialog").dialog('close');
	}
	
	$(document).ready(function(){ 
		$("#uploadDialog,#updateDialog").dialog({
			modal: true,
			autoOpen: false,
			height: 'auto',
			width: 400,
			title: "Upload Image",
			open: function()
			{
				$('.ui-widget-overlay').bind('click', function()
				{
					$("#uploadDialog").dialog('close');
				});
			},
			show: {
				effect: "blind",
				duration: 700
			},
			hide: {
				effect: "explode",
				duration: 1000
			}
		});
		
		$("#uploadForm,#updateForm").submit(function(e) {
			e.preventDefault();    
			var formData = new FormData(this);		
			$.ajax({
				url: window.location.pathname,
				type: 'POST',
				data: formData,
				success: function (data) {
					alert(data); //alert the output
					fetchUploads(); //refresh the list
				},
				cache: false,
				contentType: false,
				processData: false
			});
		});


		$('button.upload').click(function(event){
			$("#uploadDialog").dialog("open");
		});
		
		fetchUploads();
	});
</script>

<style>
	html, body {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
		font-weight: normal;
		padding: 0;
		margin: 0;	
	}
	
	button {
		border-radius: 5px;
		border: none;
		padding: 10px 0;
		background: darkcyan;
		color: #ffffff;
		font-weight: bold;
		width: 75px;
		margin: 5px 5px 5px 0;
		cursor: pointer;
	}
	
	.control-panel {
		display: block;		
	}
	
	.table-container {
		display: block;
		margin: 0 auto;
		width: 100%;
		max-width: 800px;
		text-align: left;
	}
	
	table.list {
		width: 100%;
		border: 1px solid rgba(0,0,0,0.25);
		box-shadow: 5px 5px 10px rgba(0,0,0,0.225);		
	}
	
	table.list thead {
		background: #9f9f9f;
		border-bottom: 1px solid rgba(0,0,0,0.25);;
	}
	
	table.list thead td {
		padding: 10px;
		color: #ffffff;
	}
	
	table.list tbody td {
		padding: 5px;
	}
	
	table.list tbody tr:nth-child(even) {
		background-color: #f2f2f2;
	}
	
	.media-photo-container {
		display: inline-block;
		width: 100px;
		height: 100px;
		margin: 0;
		padding: 0;
		text-decoration: none;
		overflow: hidden;
		position: relative;
		box-shadow: 3px 3px 5px rgba(0,0,0,0.125);
	}
	
	.media-photo {
		display: block;
		max-width: 150px;
		height: auto;
	}
	
	.media-photo.dialog {
	    padding: 5px;
		border: 1px solid #9f9f9f;
		margin: 10px 0 20px 0;
	}
	
	.ui-dialog {
		background: #ffffff;
		border: 1px solid #6f6f6f;		
	    box-shadow: 5px 5px 10px rgba(0,0,0,0.25);		
	}
	
	.ui-dialog-titlebar {
		background: #9f9f9f;
		color: #ffffff;
		margin-bottom: 10px;
		padding: 0 10px;
		height: 45px;
	}
	
	.ui-dialog-title {
		font-weight: bold;
		line-height: 45px;
		display: inline-block;
	}
	
	.ui-dialog-titlebar button {
		float: right;
	}
	
	.ui-dialog-content {
		padding: 5px;
	}
	
	.form-group {
	
	}
	
	.form-group label {
		padding: 5px;
		margin: 0 5px 10px 0;
		display: inline-block;	
	}
	
	.form-group input {
		border: 1px solid #9f9f9f;
		color: #333;
		padding: 5px 10px;
		border-radius: 3px;	
	}
	
	#selectImage {
		margin-left: 5px;
	}
</style>
</head>
<body>	
    <div class="table-container">
    	<h1>Image Uploader</h1>
        <div class="control-panel">
            <button type="button" class="btn btn-success btn-filter upload" title="Upload file">Upload</button>
        </div>
        <table class="list" cellpadding="2" cellspacing="0" align="center">
        <thead>
            <tr>
                <td width="200">Title</td>
                <td>Thumbnail</td>
                <td>Filename</td>
                <td>Date</td>
                <td>&nbsp;</td>
            </tr>
        </thead>
        <tbody id="list-body">
        </tbody>
        </table>
    </div>
    
	<!-- Dialog Boxes -->
    <div id="uploadDialog" title="Upload Media" style="display: none;">	
        <div class="container">
            <form id="uploadForm" action="./" method="post" enctype="multipart/form-data">
                <input id="action" type="hidden" name="action" value="upload" />                
                <div class="container form-group"> 	  
                	<label for="title">Title</label>
                    <input type="text" name="title" value="" required />
                    <div id="selectImage">
                        <input type="file" name="file" id="file" required class="button" />
                        <input type="submit" name="submit" value="Upload" class="button" />
                    </div>                        
                </div>
            </form>
        </div>
    </div>
    
    
    <!-- Dialog Boxes -->
    <div id="updateDialog" title="Upload Media" style="display: none;">	
        <div class="container">
            <form id="updateForm" action="./" method="post" enctype="multipart/form-data">
                <input id="action" type="hidden" name="action" value="update" />
                <input id="id" type="hidden" name="id" value="" />                
                <div class="container form-group"> 	  
                	<label for="title">Title</label>
                    <input type="text" name="title" id="title" required value="" />                    
                    <img id="thumb" class="media-photo dialog" src="" />
                    <div id="selectImage">
                        <input type="file" name="file" id="file" class="button" />
                        <input type="submit" name="submit" value="Update" class="button" />
                    </div>                        
                </div>
            </form>
        </div>
    </div>
    

</body>
</html>
